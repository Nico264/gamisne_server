#ifndef GAMEINSTANCE_H
#define GAMEINSTANCE_H

#include <QObject>
#include <QTcpSocket>
#include <QUuid>
#include <QDataStream>

enum States {
    Searching,
    WantsPair,
    MayPair,
    Playing,
    Finished
};

enum MessageTypes {
    NewName,
    AskPair,
    Accept,
    Refuse,
    Forget,
    Finish
};

class GameInstance : public QObject
{
    Q_OBJECT
public:
    GameInstance(QTcpSocket*, QList<GameInstance*>*);
    ~GameInstance();

    GameInstance* opponent = nullptr;
    int state = Searching;

    void send(int, const QVariantList& = {});
    void broadcast(int, const QVariantList&);

signals:
    void finished();

private slots:
    void incommingMessage();

private:
    QTcpSocket* connection;
    GameInstance* getPlayerByUUID(QUuid);
    QList<GameInstance*>* searchingPlayers;

    QString name = "Unknown player";
    QUuid id;
    QDataStream in;
};

#endif // GAMEINSTANCE_H
