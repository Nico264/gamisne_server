#include <QCoreApplication>
#include <QTcpServer>
#include "gameinstance.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    auto *searchingPlayers = new QList<GameInstance*>();

    auto *server = new QTcpServer();
    server->listen(QHostAddress::Any, 8080);
    QObject::connect(server, &QTcpServer::newConnection, [server, searchingPlayers](){
        QTcpSocket *socket = server->nextPendingConnection();
        new GameInstance(socket, searchingPlayers);
    });

    return a.exec();
}
