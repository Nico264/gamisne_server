#include <QDebug>

#include "gameinstance.h"

GameInstance::GameInstance(QTcpSocket* socket, QList<GameInstance*>* players)
{
    connection = socket;
    id = QUuid::createUuid();
    searchingPlayers = players;
    in.setDevice(connection);
    in.setVersion(QDataStream::Qt_5_6);

    foreach (GameInstance *player, *searchingPlayers) {
        this->send(NewName, {player->id, player->name});
    }
    broadcast(NewName, {id, name});

    searchingPlayers->append(this);

    connect(connection, &QTcpSocket::readyRead, this, &GameInstance::incommingMessage);
    connect(connection, &QTcpSocket::disconnected, this, &GameInstance::finished);
    connect(this, &GameInstance::finished, this, &GameInstance::deleteLater);
}

GameInstance::~GameInstance() {
    if (opponent != nullptr && state != Finished) {
        opponent->opponent = nullptr;
        opponent->send(Refuse);
        opponent->state = Searching;
        searchingPlayers->append(opponent);
        opponent->broadcast(NewName, {opponent->id, opponent->name});
    } else {
        searchingPlayers->removeOne(this);
    }
    broadcast(Forget, {id});
    connection->deleteLater();
}

GameInstance* GameInstance::getPlayerByUUID(QUuid uuid){
    foreach (GameInstance* player, *searchingPlayers) {
        if (player->id == uuid) {
            return player;
        }
    }
    return nullptr;
}

void GameInstance::broadcast(int messageType, const QVariantList& messageContent){
    foreach (GameInstance *player, *searchingPlayers) {
        if (this->id != player->id) {
            player->send(messageType, messageContent);
        }
    }
}

void GameInstance::send(int messageType, const QVariantList& messageContent) {
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);

    out << messageType << messageContent;

    connection->write(block);
    qDebug() << "→" << name << messageType << messageContent;
}

void GameInstance::incommingMessage(){
    while (!connection->atEnd()) {
        in.startTransaction();

        int messageType;
        QVariantList payload;
        in >> messageType >> payload;

        if (!in.commitTransaction())
            return;

        qDebug() << "←" << name << messageType << payload;

        switch (messageType) {
        case NewName:
            if (payload.length() == 1) {
                this->name = payload[0].toString();
                broadcast(NewName, {this->id, this->name});
            } else {
                qDebug() << "Malformed message";
            }
            break;
        case AskPair:
            if (payload.length() == 5) {
                GameInstance *askedOpponent = getPlayerByUUID(payload[0].toUuid());
                if (askedOpponent != nullptr && askedOpponent != this) {
                    this->opponent = askedOpponent;
                    this->state = WantsPair;
                    opponent->opponent = this;
                    opponent->state = MayPair;
                    searchingPlayers->removeOne(this);
                    searchingPlayers->removeOne(opponent);
                    opponent->send(AskPair, QVariantList({id, name}) << payload.mid(1, 4));
                    this->broadcast(Forget, {this->id});
                    opponent->broadcast(Forget, {opponent->id});
                } else {
                    this->send(Refuse);
                    qDebug() << "Can't pair: connection terminated";
                }
            } else {
                qDebug() << "Malformed message";
            }
            break;
        case Refuse:
            if (state == MayPair) {
                opponent->opponent = nullptr;
                opponent->send(Refuse);
                opponent->state = Searching;
                searchingPlayers->append(opponent);
                searchingPlayers->append(this);
                opponent->broadcast(NewName, {opponent->id, opponent->name});
                this->broadcast(NewName, {this->id, this->name});
                this->opponent = nullptr;
                this->state = Searching;
            } else {
                qDebug() << "Unconsistant refusal";
            }
            break;
        case Accept:
            if (payload.length() == 4) {
                if (state == MayPair) {
                    opponent->send(Accept, payload.mid(0, 4));
                    opponent->state = Playing;
                    this->state = Playing;
                } else {
                    qDebug() << "Unconsistant approval";
                }
            } else {
                qDebug() << "Malformed message";
            }
            break;
        case Finish:
            if (payload.length() == 1) {
                if (state == Playing) {
                    this->state = Finished;
                    opponent->send(Finish, {payload[0]});
                    if(opponent->state == Finished){
                        emit opponent->finished();
                        emit this->finished();
                    }
                } else {
                    qDebug() << "Unconsistant finish";
                }
            } else {
                qDebug() << "Malformed message";
            }
            break;
        default:
            qDebug() << "Unrecognized message…";
            break;
        }
    }
}
